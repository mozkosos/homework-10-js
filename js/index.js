document.addEventListener('DOMContentLoaded', function(){
    const labels = document.querySelectorAll('.input-wrapper')

    labels.forEach(label =>{
        const btn = label.querySelector( '.button')
        btn.addEventListener('click', ()=>{


            if (label.classList.contains('showPassword')){
                label.classList.remove('showPassword')
                btn.previousElementSibling.setAttribute('type', 'password')
            }
            else {
                label.classList.add('showPassword')
                btn.previousElementSibling.setAttribute('type', 'text')

            }
        })
    })
    const confirmBtn = document.querySelector('.btn')
    const alertMessage = document.querySelector('.alert-message')

    confirmBtn.addEventListener('click', () =>{
        const passwordFirst = document.querySelector('.password-first')
        const passwordCheck = document.querySelector('.password-check')
        if (passwordFirst.value === passwordCheck.value){alert('You are welcome')

        }else {

            alertMessage.innerHTML = 'Нужно ввести одинаковые значения'
            alertMessage.style.color = 'red'
        }

        })
});